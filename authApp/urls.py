from django.urls import path

from .views import homepage, loginView, logoutView

urlpatterns = [
    path('', homepage, name="homepage"),
    path('login/', loginView, name="login"),
    path('logout', logoutView, name="logout"),
]
